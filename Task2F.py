# -*- coding: utf-8 -*-
"""
Created on Thu Feb  2 15:00:09 2017

@author: Chris
"""

import datetime
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels, plot_multi_water_levels
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.analysis import polyfit
from floodsystem.plot import plot_water_level_with_fit

def run():
    """Requirements for Task 2F"""

    # Build list of stations
    stations = build_station_list()
    update_water_levels(stations)
     
    #List of 5 most at risk stations    
    risk_list = stations_highest_rel_level(stations, 5)
    dt = 2 #2 days back
    polynum = 4 #polynomial degree 4
    
    #Cycle through each station and plot each one individually
    for station in risk_list:       
        #Get past data for each station and plot
        dates, levels = fetch_measure_levels(station.measure_id,
                                             dt=datetime.timedelta(days=dt))
        #plot_water_levels(station,dates,levels)
        poly, d0 = polyfit(dates,levels,polynum)
        plot_water_level_with_fit(station, dates, levels, poly)


        

if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")

    # Run Task1A
    run()