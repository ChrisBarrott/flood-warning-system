# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 20:51:51 2017

@author: Chris
"""

from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius

def run():
    """Requirements for Task 1C"""

    # Build list of stations and centre
    stations = build_station_list()
    cambcentre = (52.2053, 0.1218)
    
    print(stations_within_radius(stations,cambcentre, 10))


if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")

    # Run Task1A
    run()