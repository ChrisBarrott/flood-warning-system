# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 19:58:57 2017

@author: Chris
"""
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance


def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = build_station_list()

    #Initialise point
    cambcentre = (52.2053,0.1218)
    
    #Get list of (station.name,station.town, distance) tuples
    StatDistList = stations_by_distance(stations, cambcentre)
    
    #Print first and last 10
    print('Closest stations:')
    for data in StatDistList[:10]:
        print(data)
    
    print('Furthest stations:')
    for data in StatDistList[-10:]:
        print(data)

if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")

    # Run Task1A
    run()