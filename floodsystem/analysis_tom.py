# -*- coding: utf-8 -*-
"""
Created on Sun Feb 12 11:36:48 2017

@author: tmull
"""

"""Using your implementation, list the towns where you assess the risk of flooding 
to be greatest. Explain the criteria that you have used in making your assessment, 
and rate the risk at ‘severe’, ‘high’, ‘moderate’ or ‘low’."""

"""Extrapolate the data to get predictions"""

from floodsystem.analysis import polyfit
from floodsystem.plot import plot_water_levels
import datetime
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.utils import sorted_by_key
import numpy as np




    
def assign_tomorrow_flood_risk(station, dt, dates, levels):    
    
        
    m = 0.4
        
    poly, d0 = polyfit(dates, levels, 4)
            
    if asses_validity_poly_curve(dates, levels, poly, dt) < 0.03:
        x = abs(poly(dt + 1)) - abs(station.typical_range[1])
                                
                                
        if x > 0:
                                    
            risk_value = 5 + m*x
                                    
        else:
            risk_value = 5 + m*x
                                    
                            
        if risk_value > 10:
            risk_value = 10
                                    
                            
        elif risk_value < 0:
            risk_value = 0
                                    
                
                        
    else:
        risk_value = None
        print("Curve not valid for extrapolation")        
                    

    
        
    return risk_value
    
def asses_validity_poly_curve(dates, levels, poly, dt):
    """Compare value given by polyfit line to that given by actual water
    level line and take an average accross the last dt days"""
    
    
    #Set up polynomial data
    #Convert dates to floats
    dates_float = matplotlib.dates.date2num(dates)
    #print(dates_float)
    
    poly_check = []

    x1 = np.linspace(dates_float[0], dates_float[-1], len(levels))
    
    x1_shift = x1 - x1[0]

     
    for i in range(len(x1_shift)):
        error = abs(poly(x1_shift[i]) - levels[i])
        
        
        poly_check.append(error)
        
    average = sum(poly_check)/len(poly_check)   
    
    return average


def cur_level_to_typical_range():
    
    x = station.relative_water_level
    
    risk_value_2 = abs(5*x)
    
    if risk_value_2 > 10:
        risk_value_2 = 10
        
       
    
    return risk_value_2











    
