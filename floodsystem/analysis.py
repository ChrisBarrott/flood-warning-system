# -*- coding: utf-8 -*-
"""
Created on Thu Feb  2 15:00:37 2017

@author: Chris
"""

import datetime
import matplotlib
import numpy as np
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels,plot_water_level_with_fit

def polyfit(dates, levels, p):
    """For a given data set of dates and levels and a polynomial degree,
    returns a best fit line object"""
    
    #Convert dates to floats
    dates_float = matplotlib.dates.date2num(dates)
    
    #Amount to shift values by to avoid error
    dateshift = dates_float[0]
        
    # Find coefficients of best-fit polynomial f(x) of degree p
    p_coeff = np.polyfit(dates_float - dateshift, levels, p)

    # Convert coefficient into a polynomial that can be evaluated,
    poly = np.poly1d(p_coeff)

    return poly, dateshift
    
def current_level_frequency(station, levels):
    """Function that takes in a station, compares its current water level
    to past data. Returns a score 1-10 based on flood risk. If current water
    level is higher than 99% of time, then score will be high etc"""

    #Go through levels list. Increment counter every time water level was
    #less than current
    HighFreq = 0
    
    for level in levels:
        try:
            if level < station.latest_level:
                HighFreq += 1 #Increment counter
        except:
            print('Error in levels data')
        

    #Value from 0 to 1. Amount of time level has been above current in past month        

    HighFreqValue = HighFreq/len(levels)     
    #If HighValueFreq is high, high risk of flooding
    #Fit to y=10x^2 to return a score from 0 to 10
    #This curve biases risk to the lower probabilites
    return(10*HighFreqValue**2)      

    #plot_water_levels(station, dates, levels)
    
    
def average_level(station,dates, levels):
    """Takes in a station, compares its level data over past 'days'
    to typical range. Returns a score 1-10 based on flood risk"""

    #Average levels data
    LevelTotal = 0
    for level in levels:
        LevelTotal += level
    LevelAverage = LevelTotal/len(levels)

    #Compare level average to typical range. Use rel_water_level calculation
    if LevelAverage == station.typical_range[0]:
        rel_water_level = 0.0
    else:       
        rel_water_level = (LevelAverage)/(station.typical_range[1])
        
    #plot_water_levels(station, dates, levels)    
        
    #Linear relationship between rel_water_level and flood score
    if rel_water_level > 1:
        return 10
    elif rel_water_level < 0:
        return 0
    else:
        return 10*rel_water_level
        

    
def poly_derivative(station, dates, levels, p):
    """Takes in a station, days to look at and a polynomial degree. 
    Returns a score of 10 for a positive derivative of the best 
    fit line (ie increasing), 0 for decreasing"""
    
    #Calculate best fit line
    poly, d0 = polyfit(dates, levels, p)
    
    #Get dates in float form
    dates_float = matplotlib.dates.date2num(dates)
    valdif = poly(dates_float[-1]-d0) - poly(dates_float[-2]-d0)
    
    #Estimate gradient as change in x over change in y for last 2 points
    grad = valdif/(-dates_float[-1]+dates_float[-2])
     
    #print(grad)
    
    #Fit to straight line. Weighted to the low flood risk end
    #Gradient of 0 returns a score of 2
    if grad > 0:
        score = 2 + 15*grad
    else:
        score = 2 + 15*grad #Actually negative as gradient < 0
        
    if score > 10:
        score = 10
    elif score < 0:
        score = 0
        
    return score
 

def poly_extrapolate_risk(station, dt, dates, levels):    
    """Assign a flood risk depending on the best fit extrapolation"""
    
    m = 0.4    
    poly, d0 = polyfit(dates, levels, 4)
            
    if assess_validity_poly_curve(dates, levels, poly, dt) < 0.03:
        x = abs(poly(dt + 1)) - abs(station.typical_range[1])
                                                          
        if x > 0:                                    
            risk_value = 5 + m*x                                    
        else:
            risk_value = 5 + m*x                                    
                            
        if risk_value > 10:
            risk_value = 10                                    
                            
        elif risk_value < 0:
            risk_value = 0                                 
                                      
    else:
        risk_value = None
        print("Curve not valid for extrapolation")        
  
    return risk_value

    
def assess_validity_poly_curve(dates, levels, poly, dt):
    """Compare value given by polyfit line to that given by actual water
    level line and take an average accross the last dt days"""
    
    #Set up polynomial data
    #Convert dates to floats
    dates_float = matplotlib.dates.date2num(dates)
    #print(dates_float)
    
    poly_check = []
    x1 = np.linspace(dates_float[0], dates_float[-1], len(levels))    
    x1_shift = x1 - x1[0]
     
    for i in range(len(x1_shift)):
        error = abs(poly(x1_shift[i]) - levels[i])     
        poly_check.append(error)
        
    average = sum(poly_check)/len(poly_check)       
    return average


def rel_water_level_risk(station):
    """Asigns a risk value of the current relative water level"""
    x = station.relative_water_level()   
    risk_value_2 = abs(5*x)
    
    if risk_value_2 > 10:
        risk_value_2 = 10

    return risk_value_2
    

        
    
    
        
    
   
   
   