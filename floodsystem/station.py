"""This module provides a model for a monitoring station, and tools
for manipulating/modifying station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None
        self.flood_risk = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += " measure id: {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d
        
    def typical_range_consistent(self):
        """Returns true if data is consistent or false if data is
        inconsistent or unavailable"""
        
        #initialise boolean variable
        result = True
        
        #check for consistency
        if self.typical_range is None:
            #Checks there are values in each slot
            result = False
        elif self.typical_range[0] > self.typical_range[1]:
            #Checks lower value is less than higher value
            result = False
        elif isinstance(self.latest_level, list):
            #If error in latest level data causing it to be list
            result = False
        
        return result
        
        
    def relative_water_level(self):
        """Returns latest water level as a fraction of the typical range
        0.0 = typical low, 1.0 = typical high"""
         
        rel_water_level = 1
        
        if self.typical_range_consistent() and self.latest_level != None:
            if self.latest_level == self.typical_range[0]:
                rel_water_level = 0.0
                
                
            else:
                
                rel_water_level = (self.latest_level)/(self.typical_range[1])
                
        else:
            rel_water_level = None
             
        return rel_water_level    
    
    def set_flood_risk(self, score):
        """Sets the internal variable of the flood risk"""        
        self.flood_risk = score
         
         
        
def inconsistent_typical_range_stations(stations):
    """Returns list of stations from input stations that have 
    inconsistent range data"""
    
    incon_stations = []
    
    for station in stations:
        if not station.typical_range_consistent(): #If inconsistent 
            incon_stations.append(station)
            
    return incon_stations

            
        