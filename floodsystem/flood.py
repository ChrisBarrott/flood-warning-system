# -*- coding: utf-8 -*-
"""
Created on Sat Jan 28 13:06:42 2017

@author: tmull
"""


from .station import MonitoringStation
from .utils import sorted_by_key

def stations_level_over_threshold(stations, tol):
    """Returns a list of tuples containing the station and the relative latest
    water level"""
    flood_risk_stat = []
    
    for station in stations:
        water_level = station.relative_water_level()
       # print(station.name, water_level)
       
        if (not water_level is None) and (water_level > tol):
            
            tuple_ = (station, water_level)           
            flood_risk_stat.append(tuple_) 
       
       
       
  #  for station in stations:
  #      
  #      #water_level = station.relative_water_level()
  #      #print(station.name, water_level)
  #      
  #      if station.relative_water_level() is None:
  #          break
  #      
  #      elif station.relative_water_level() > tol:
  #          
  #          tuple_ = (station, station.relative_water_level())
  #          
  #          flood_risk_stat.append(tuple_) 
        
  
    sorted_list = sorted_by_key(flood_risk_stat, 1, True)    
        
    return sorted_list    
            
    
def stations_highest_rel_level(stations, N):
    """Return a list of the N stattions at which the water level relative to
    to the typical range is highest"""
    
    flood_risk_stat = []
    
    for station in stations:
        water_level = station.relative_water_level()
       
       
        if (not water_level is None):
            
            tuple_ = (station, water_level)           
            flood_risk_stat.append(tuple_) 
       
    #Sort list of tuples by rel water level (tuple[1])   
    sorted_list = sorted_by_key(flood_risk_stat, 1, True) 
    
    #Establish new list
    sliced_list = []
    for tuple_ in sorted_list[:N]:
        #Add station only to list
        sliced_list.append(tuple_[0])
    
    
    return sliced_list

    


    
    
    
    
    
    
    
    
    
    
    
    
