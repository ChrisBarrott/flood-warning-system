# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 17:05:54 2017

@author: Chris
"""

import math
from bokeh.io import output_file, show
from bokeh.plotting import figure
from bokeh.tile_providers import CARTODBPOSITRON
from bokeh.models import (
  GMapPlot, GMapOptions, ColumnDataSource, Circle, DataRange1d, PanTool, WheelZoomTool, BoxSelectTool
)

def merc_x(lon):
  """Takes in lon and returns mercator coord"""
  r_major=6378137.000
  return r_major*math.radians(lon)

def merc_y(lat):
  if lat>89.5:lat=89.5
  if lat<-89.5:lat=-89.5
  r_major=6378137.000
  r_minor=6356752.3142
  temp=r_minor/r_major
  eccent=math.sqrt(1-temp**2)
  phi=math.radians(lat)
  sinphi=math.sin(phi)
  con=eccent*sinphi
  com=eccent/2
  con=((1.0-con)/(1.0+con))**com
  ts=math.tan((math.pi/2-phi)/2)/con
  y=0-r_major*math.log(ts)
  return y


def plot_towns(townslist):
    """Takes in (town,risk,coord,num) tuples in a list and plots
    on a tile map"""
    

    bound = 10000 # meters
    fig = figure(plot_width = 1000, plot_height = 1000,
                 tools='pan, wheel_zoom', x_range=(-1e+6,0.3e+6), 
                 y_range=(6.5e+6,8e+6))
    
    
    #Assign lat, long variables
    lat, lon = [],[]    
    for town in townslist:
        lat.append(merc_y(town[2][0]) + 0.0322e+6)#*1e+5)
        lon.append(merc_x(town[2][1]))#*1e+5)
    
    #Assign colours using css standard
    colour_choice = ["lime","lawngreen","greenyellow","yellow","gold","coral","orange","oragered","crimson","red"]   
    #colour_choice = [(0,255,0,0.6), (60,255,0,0.6), (120,255,0,0.6),(180,255,0,0.6),(255,255,0,0.6), (255,180,0,0.6), (255,120,0,0.6),(255,60,0,0.6),(255,0,0,0.6),(0,0,0,0.6)]
    colour_list = []
    for town in townslist:
        colour_list.append(colour_choice[int(round(town[1])-1)])  

    #Assign circle size
    circle_size = []
    for town in townslist:
        circ_size = town[3]*7
        if circ_size >= 35: #Limit size
            circ_size = 35        
        circle_size.append(circ_size)
    
    #Assign variables to js equivilent
    source = ColumnDataSource(
        data=dict(
            lat=lat,
            lon=lon,
            colour_list = colour_list,
            circle_size = circle_size
        )
    )      

    circle = Circle(x="lon", y="lat", size="circle_size", fill_color="colour_list", fill_alpha=0.75, line_color=None)
    fig.add_glyph(source, circle)
    
    
    fig.axis.visible = True
    fig.add_tile(CARTODBPOSITRON)
    output_file("Flood_risk.html")
    show(fig)

    

    