"""This module contains a collection of functions related to
geographical data.

"""
from floodsystem.stationdata import build_station_list
from collections import defaultdict
from math import radians, cos, sin, asin, sqrt
from floodsystem.utils import sorted_by_key

AVG_EARTH_RADIUS = 6371  # in km

def rivers_with_station(stations):
    """Gives a list of all rivers where a station lies"""
    
    rivers = set() #will only put one river in the list even if there are 
                   #multiple stations
    print(type(rivers))
    
    for station in stations:
       rivers.add(station.river)
       
    return rivers   
        
def stations_by_river(stations):
    """maps river names to the stations on that river"""
    
    rivers_to_stations = {} #create a dictionary 

    for station in stations:
        river = station.river
        
        if river in rivers_to_stations:
            station_list = rivers_to_stations.get(river) #this takes the list
            #corresponding to that station
            
            
            station_list.append(station.name) #adds station name to station_list
            
            rivers_to_stations[river] = station_list #adds river and station list 
            #back to rivers_to_stations
                
        
        else:
            rivers_to_stations[river] = [station.name]#if river not in in rivers_to_stations
            #adds river and station name (in a list) to the dictionary
            

    return rivers_to_stations


def stations_by_town(stations):
    """Takes in a list of stations and sorts into a
    dictionary where towns corresspond stations"""
    
    towns_to_stations = {} #create a dictionary 

    #Cycle through list of stations to create town to station dictionary
    for station in stations:
        town = station.town
        
        if town in towns_to_stations:
            station_list = towns_to_stations.get(town) #this takes the list
            #corresponding to that station
            
            
            station_list.append(station) #adds station to station_list
            
            towns_to_stations[town] = station_list #adds river and station list 
            #back to towns to stations
                
        
        else:
            towns_to_stations[town] = [station]#if river not in in rivers_to_stations
            #adds river and station name (in a list) to the dictionary       

    return towns_to_stations


def floodrisk_by_town(towns_to_stations):
    """Takes in a towns to station dictionary and returns a 
    list of (town, floodrisk, coord, stationnum) tuples. Averages the coords
    of the stations to get towns"""
    
    #Cycle through towns_to_stations and create a list of (town,floodrisk)
    towns_risk_list = []
    for town in towns_to_stations:
        try:
            total = 0
            i = 0
            totallat = 0
            totallon = 0
            for station in towns_to_stations[town]:
                total += station.flood_risk
                totallat += station.coord[0]
                totallon += station.coord[1]
                i += 1
            #Average the flood risk and coordinate
            avg_risk = total/i
            coord = (totallat/i,totallon/i)
            
            towns_risk_list.append((town,avg_risk,coord,i))
        except:
            print('Error in data. Not appropriate for graph')
    return towns_risk_list


    
def rivers_by_station_number(stations, N):
    """Puts each river in a tuple with the corresponding number of stations
    on that river. Then it ranks these tuples by number of stations from 
    highest to lowest"""
    rivers_to_numstations = {}
    
    for station in stations:
        river = station.river
        
        if river in rivers_to_numstations:
            rivers_to_numstations[river] += 1 #adds 1 to the value
                
        
        else:
            rivers_to_numstations[river] = 1 #assigns a value of to the river 
            #then adds it to the dictionary


    list_of_tuples = rivers_to_numstations.items() #unzips dictionary into a 
    #list of tuples
    
    
    
    sorted_tuples = sorted_by_key(list_of_tuples, 1, True)
    #sorts list of tuples by value highest to lowest
    
    
    
    a = sorted_tuples[:N]

    
    #adds extra rivers if they have the same number of stations
    for i in sorted_tuples[N:]:
        if i[1] == a[-1][1]: 
            a.append(i)
            
        else:
            break
     
    
    return a
   
    
def haversine(point1, point2, miles=False):
    """ Calculate the great-circle distance between two points on the Earth surface.
    :input: two 2-tuples, containing the latitude and longitude of each point
    in decimal degrees.
    Example: haversine((45.7597, 4.8422), (48.8567, 2.3508))
    :output: Returns the distance bewteen the two points.
    The default unit is kilometers. Miles can be returned
    if the ``miles`` parameter is set to True.
    """
    # unpack latitude/longitude
    lat1, lng1 = point1
    lat2, lng2 = point2

    # convert all latitudes/longitudes from decimal degrees to radians
    lat1, lng1, lat2, lng2 = map(radians, (lat1, lng1, lat2, lng2))

    # calculate haversine
    lat = lat2 - lat1
    lng = lng2 - lng1
    d = sin(lat * 0.5) ** 2 + cos(lat1) * cos(lat2) * sin(lng * 0.5) ** 2
    h = 2 * AVG_EARTH_RADIUS * asin(sqrt(d))
    if miles:
        return h * 0.621371  # in miles
    else:
        return h  # in kilometers


        
def stations_by_distance(stations, p, MaxDist = False, radius = 0):
    """Given list of stations and coord p reyirms a list of
    (station, distance) tuples sorted by distance.
    If MaxDist = true then it only adds stations to the list with a 
    distance < than radius
    """
    
    #List of (station name, station town, distance) tuples
    StatDistList = []
    
    #Calc distance between and add to list in tuples
    for station in stations:
        if not MaxDist:
            StatDistTuple = (station.name, station.town,haversine(p,station.coord))
            StatDistList.append(StatDistTuple)
        
        # Only add to list stations that are < r away    
        else:
            if haversine(p,station.coord) <= radius:
                StatDistTuple = (station.name, station.town,haversine(p,station.coord))
                StatDistList.append(StatDistTuple)    
 
    #Sort into distance
    StatDistList.sort(key=lambda x: x[2])
        
    return StatDistList
    
def stations_within_radius(stations, centre, r):
    """Returns a list of all station.name within a certain radius r of point 
    centre"""
    
    #Get list of (stations,distance) within distance r
    StatDistList = stations_by_distance(stations, centre, True, r)
    
    #Form list of names from StatDistList
    StatRadListName = []
    for i in StatDistList:    
        StatRadListName.append(i[0])
    
    #Sort into alphabetical
    StatRadListName.sort()
    
    return StatRadListName    
    


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    