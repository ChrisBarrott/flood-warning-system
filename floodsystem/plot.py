# -*- coding: utf-8 -*-
"""
Created on Fri Jan 27 18:13:27 2017

@author: Chris
"""

#Module responsible for the plotting of station data
import datetime
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from floodsystem.datafetcher import fetch_measure_levels

def plot_water_levels(station,dates,levels):
    """Function that displays a plot of water level against time for
    a station"""
    
    #Set up low and high values
    low = [station.typical_range[0]]
    high = [station.typical_range[1]]
    
    # Plot
    plt.figure() #This creates a new plot
    plt.plot(dates, levels)
    
    #Plot high and low lines
    plt.plot((dates[0], dates[-1]), (low, low), 'g-')
    plt.plot((dates[0], dates[-1]), (high, high), 'r-')

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('Date/Time')
    plt.ylabel('Water Level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name)
    
    #Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()
     
    
def plot_multi_water_levels(stationlist, dt):
    """Function that displays a plot of water level against
    time for a list of stations for days before"""
    
    newdt = datetime.timedelta(days=dt)
    stationnamestring = ''
    
    #Cycle through stations and plot each one
    for station in stationlist:
        dates, levels = fetch_measure_levels(station.measure_id,
                                             newdt)
        plt.plot(dates,levels, label = station.name)
        
        #Add to title string
        stationnamestring += station.name + ', '
                
    #Admin    
    plt.xlabel('Date/Time')
    plt.ylabel('Water Level (m)')
    plt.xticks(rotation=45);
    plt.title(stationnamestring[:-2]) #To not display final comma    
    plt.legend()
    
    #Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()

     
def plot_water_level_with_fit(station, dates, levels, p):
    """plots a graph for level against date for a station with the 
    best fit polynomial p"""
    
    #Set up low and high values
    low = [station.typical_range[0]]
    high = [station.typical_range[1]]
    
    #Set up polynomial data
    #Convert dates to floats
    dates_float = matplotlib.dates.date2num(dates)
    
    #Amount to shift values by to avoid error
    dateshift = dates_float[0]
        
    # Plot polynomial fit at 30 points along interval, with date shift
    x1 = np.linspace(dates_float[0], dates_float[-1], 30)
    plt.plot(x1, p(x1-dateshift), label = 'Polynomial best fit')
    
    #Plot level data
    plt.plot(dates, levels, label = station.name)
    
    #Plot high and low lines
    plt.plot((dates[0], dates[-1]), (low, low), 'g-')
    plt.plot((dates[0], dates[-1]), (high, high), 'r-')

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('Date/Time')
    plt.ylabel('Water Level (m)')
    plt.xticks(rotation=45);
    plt.title(station.name + ' with polynomial best fit line')
    plt.legend()
    
    #Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    plt.show()