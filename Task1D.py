# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 18:54:08 2017

@author: tmull
"""


from floodsystem.geo import rivers_with_station
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_river

def run():
    """Requirements for Task1D part 1"""

    stations = build_station_list()
    
    #Build list of stations
    river_list = rivers_with_station(stations)
    
    

    #Count number of stations and print
    a = sorted(river_list)
    
    #print(len(a))
    
    first_10_entries = a[:9]
    #print(first_10_entries)
    
    print(first_10_entries)


if __name__ == "__main__":
    run()

def run():
    """Requirements for Task1D part 2"""
    
    stations = build_station_list()
    river_station = stations_by_river(stations)
 
    
    
    select_a_river = river_station.get('River Cam')
    
    select_a_river.sort()
    
    #print(select_a_river)

if __name__ == "__main__":
    run()