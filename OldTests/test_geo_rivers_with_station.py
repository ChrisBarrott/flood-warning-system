# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 21:12:01 2017

@author: tmull
"""

import pytest
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station


def test_rivers_with_station ():
    
    """Tests rivers_with_station function"""
    
    stations = build_station_list()
    
    #Build list of stations
    river_list = rivers_with_station(stations)
    
    

    #sort the list into alphabetical order
    a = sorted(river_list)
    
    print(len(a))
    
    #select the first river that has a station on it
    
    first_entry = a[0]

    #compare with the with the known first station
    
    assert first_entry == 'Addlestone Bourne'