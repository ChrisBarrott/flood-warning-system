# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 20:33:24 2017

@author: Chris
"""

import pytest
from floodsystem.stationdata import build_station_list
from floodsystem.geoChris import stations_by_distance


def test_stations_by_distance():
    """Tests stations by distance function"""
   
    #Make a list
    stations = build_station_list()
    cambcentre = (52.2053,0.1218)
    StatDistList = stations_by_distance(stations, cambcentre)
    
    #Compare with known distance to closest station
    assert round(StatDistList[0][2],2) == 0.84
    