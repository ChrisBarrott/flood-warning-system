# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 23:16:14 2017

@author: tmull
"""

import pytest
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number

def test_rivers_by_station_number():
    """Tests rivers_by_station_number function"""
    
    stations = build_station_list()
    
    N = 9
    
    river_numstations = rivers_by_station_number(stations, N)
    
    #Test the first entry using the coded function
    first_entry = river_numstations[0]

    assert first_entry == ('Thames', 55)

    