# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 20:33:24 2017

@author: Chris
"""

import pytest
from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations


def test_stations_by_distance():
    """Tests station range data consistency function"""
   
    #Generate 3 stations
    
    #Consistent
    ConStation = MonitoringStation(station_id='a',
                                  measure_id='b',
                                  label='c',
                                  coord=(23,
                                         45),
                                  typical_range=(3,5),
                                  river='camb',
                                  town='cambridge')
    
    #Inconsistent                              
    InconStation1 = MonitoringStation(station_id='d',
                                  measure_id='e',
                                  label='f',
                                  coord=(23,
                                         45),
                                  typical_range=(4,2),
                                  river='camb',
                                  town='cambridge') 
                                
    #Inconsistent                              
    InconStation2 = MonitoringStation(station_id='g',
                                  measure_id='h',
                                  label='i',
                                  coord=(23,
                                         45),
                                  typical_range=None,
                                  river='camb',
                                  town='cambridge')                                
                                  
    result1 = ConStation.typical_range_consistent()
    result2 = InconStation1.typical_range_consistent()
    result3 = InconStation2.typical_range_consistent()
                                
    #Check that consistency function has the correct results
    assert result1 == True
    assert result2 == False
    assert result3 == False