# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 22:37:05 2017

@author: Chris
"""

import pytest
from floodsystem.stationdata import build_station_list
from floodsystem.geoChris import stations_within_radius


def test_stations_by_distance():
    """Tests stations by radius function"""
   
    #Make a list
    stations = build_station_list()
    cambcentre = (52.2053,0.1218)
    StatRadListName = stations_within_radius(stations,cambcentre, 10)
    
    #Compare with known close station that is first alphabetically
    assert StatRadListName[0] == 'Bin Brook'