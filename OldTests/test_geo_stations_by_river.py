# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 21:32:26 2017

@author: tmull
"""

import pytest
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_river



def test_staions_by_river ():
    """Tests rivers_with_station function"""
    
    stations = build_station_list()
    river_station = stations_by_river(stations)
 
    
    
    select_a_river = river_station.get('River Aire')
    
    select_a_river.sort()
    
    first_entry = select_a_river[0]
    
    
    #compare first_entry with the first station alphabetically on the river aire
    
    assert first_entry == 'Airmyn'
    