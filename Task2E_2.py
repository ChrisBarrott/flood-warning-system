# -*- coding: utf-8 -*-
"""
Created on Fri Jan 27 18:15:18 2017

@author: Chris
"""
import datetime
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels, plot_multi_water_levels
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_level_over_threshold

def run():
    """Requirements for Task 2E with optional extension"""

    # Build list of stations
    stations = build_station_list()
    update_water_levels(stations)
     
    
    
    
    
    #USE 2C TO GET TOP 5 AT RISK STATIONS
    
    #THIS IS A BODGE UNTIL THAT WORKS
    
    
    
    #Get list of all stations, in descending order of risk
    risk_list = stations_level_over_threshold(stations, 0.5)

    #for tuple_ in risk_list:
    #    print(tuple_[0].name, ' ', str(tuple_[1]))
    
    dt = 10
    station_list = []
    
    for tuple_ in risk_list[:5]:
        station_list.append(tuple_[0]) 
        
    plot_multi_water_levels(station_list, dt)    
    
    
        
    ######################################
    






if __name__ == "__main__":
    print("*** Task 2E - 2: CUED Part IA Flood Warning System ***")

    # Run Task1A
    run()