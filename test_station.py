"""Unit test for the station module"""

import pytest
from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations

def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town
    

def test_typical_range_consistent():
    """Tests station range data consistency function"""
   
    #Generate 3 stations
    
    #Consistent
    ConStation = MonitoringStation(station_id='a',
                                  measure_id='b',
                                  label='c',
                                  coord=(23,
                                         45),
                                  typical_range=(3,5),
                                  river='camb',
                                  town='cambridge')
    
    #Inconsistent                              
    InconStation1 = MonitoringStation(station_id='d',
                                  measure_id='e',
                                  label='f',
                                  coord=(23,
                                         45),
                                  typical_range=(4,2),
                                  river='camb',
                                  town='cambridge') 
                                
    #Inconsistent                              
    InconStation2 = MonitoringStation(station_id='g',
                                  measure_id='h',
                                  label='i',
                                  coord=(23,
                                         45),
                                  typical_range=None,
                                  river='camb',
                                  town='cambridge')                                
                                  
    result1 = ConStation.typical_range_consistent()
    result2 = InconStation1.typical_range_consistent()
    result3 = InconStation2.typical_range_consistent()
                                
    #Check that consistency function has the correct results
    assert result1 == True
    assert result2 == False
    assert result3 == False
    