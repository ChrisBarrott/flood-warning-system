# -*- coding: utf-8 -*-
"""
Created on Thu Jan 19 20:51:51 2017

@author: Chris
"""

from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations

def run():
    """Requirements for Task 1F"""

    stations = build_station_list()
    
    #Get list of inconsistent stations
    incon_stations = inconsistent_typical_range_stations(stations)
    
    #Go through list and make new list of station names
    incon_stations_names = []
    for station in incon_stations:
        incon_stations_names.append(station.name)
        
    #Sort into alphabetical    
    incon_stations_names.sort()
    
    print(incon_stations_names)
    
    
    
if __name__ == "__main__":
    print("*** Task 1A: CUED Part IA Flood Warning System ***")

    # Run Task1A
    run()