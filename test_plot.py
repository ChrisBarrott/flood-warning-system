# -*- coding: utf-8 -*-
"""
Created on Wed Feb  1 14:44:02 2017

@author: CBarrott
"""

import pytest
import datetime
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.plot import plot_water_levels, plot_water_level_with_fit
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.analysis import polyfit


def test_plot_water_level():
    """Tests that plot water level works for a range of data"""
    
    stations = build_station_list()
    update_water_levels(stations)
    
    for station in stations:
        station.set_flood_risk(station.relative_water_level())
        
    stationsnew = []
    for station in stations:
        if not station.flood_risk is None:
            stationsnew.append(station)
        
    stationsnew.sort(key=lambda station: station.flood_risk, reverse=True)
    
    #Test for highest risk, lowest risk and a mid value
    i = 0
    for station in stationsnew:
        if (i==1) or (i==900) or (i==len(stationsnew)-1):   
            dates,levels = fetch_measure_levels(station.measure_id,
                                                 dt=datetime.timedelta(days=2))    
            plot_water_levels(station, dates, levels)
        i += 1
 
    
def test_plot_water_level_with_fit():
    """Tests that plot water level with fit works for a range of data"""
    stations = build_station_list()
    update_water_levels(stations)
    
    for station in stations:
        station.set_flood_risk(station.relative_water_level())
        
    stationsnew = []
    for station in stations:
        if not station.flood_risk is None:
            stationsnew.append(station)
        
    stationsnew.sort(key=lambda station: station.flood_risk, reverse=True)
    
    #Test for highest risk, lowest risk and a mid value
    
    for p in range (2,5):
        i = 0
        for station in stationsnew:
            if (i==1) or (i==900) or (i==len(stationsnew)-1):   
                dates,levels = fetch_measure_levels(station.measure_id,
                                                     dt=datetime.timedelta(days=2))    
                poly, d0 = polyfit(dates, levels, p)
                plot_water_level_with_fit(station, dates, levels,poly)
            i += 1