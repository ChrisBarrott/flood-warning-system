# -*- coding: utf-8 -*-
"""
Created on Wed Feb  1 14:44:02 2017

@author: tmull
"""

import pytest
from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_rel_level


def test_stations_level_over_threshold():
    """Tests stations_level_over_threshold function"""
    
    stations = build_station_list()
    update_water_levels(stations)
    
    N = 10
    
    high_risk_stat = stations_level_over_threshold(stations, N)
    
    for i in range(len(high_risk_stat)):
        for j in range(i + 1, len(high_risk_stat)):
            assert high_risk_stat[i][1] > high_risk_stat[j][1]

def test_stations_highest_rel_level():
    stations = build_station_list()
    
    update_water_levels(stations)
    
    N = 10
    
    high_risk_stat = stations_highest_rel_level(stations, N)
    
    assert high_risk_stat[0].relative_water_level() > high_risk_stat[1].relative_water_level()