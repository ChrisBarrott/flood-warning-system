# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 22:39:51 2017

@author: CBarrott
"""
import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.analysis import (current_level_frequency, average_level,poly_derivative,
poly_extrapolate_risk, rel_water_level_risk)
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels,plot_water_level_with_fit
from floodsystem.geo import stations_by_town, floodrisk_by_town
from floodsystem.utils import sorted_by_key
from floodsystem.geoplot import plot_towns


def run():
    #LIMIT = 200

    stations = build_station_list()
    update_water_levels(stations)
    
    #If data is not consistent, remove from the list
    for station in stations:
        if not station.typical_range_consistent():
            stations.remove(station)
    #stations = stations[:LIMIT]
    
    i = 0   
    for station in stations: 
        i += 1
        print(i, ' of ', len(stations)) 
        try:            
            #Get data for past 5 days
            dates5, levels5 = fetch_measure_levels(station.measure_id,
                                                 dt=datetime.timedelta(days=5))    
            
            #Get data for past day
            dates1, levels1 = fetch_measure_levels(station.measure_id,
                                                 dt=datetime.timedelta(days=1))
        
            #Initialise score list
            scores = []            
            
            #Compare current level to past data    
            scores.append(current_level_frequency(station, levels5))
                    
            #Compare average level over past 'days' to typical range
            scores.append(average_level(station, dates5, levels5))
                   
            #Look at gradient of best fit line
            p = 4
            scores.append(poly_derivative(station, dates1, levels1, p))
            
            #Look at relative water level
            scores.append(rel_water_level_risk(station))
            
            #Best fit line extrapolations
            scores.append(poly_extrapolate_risk(station, 2, dates1, levels1))
            
            #Average scores from previous functions
            print(scores)
            total = 0
            scorenum = 0
            for score in scores:
                if score is not None:
                    total += score
                    scorenum += 1
            avg_score = total/scorenum

            #Assign score to the station            
            station.set_flood_risk(avg_score)
            
        except:
            print('An error occurred with this station')
            stations.remove(station)           


    #Get towns to stations dictionary    
    towns_to_stations = stations_by_town(stations)
    #Get list of (towns, floodrisk, coord, stationum) tuples
    town_risk_list = floodrisk_by_town(towns_to_stations)
    #Sort into risk from high to low
    town_risk_list_sort = sorted_by_key(town_risk_list, 1, True)
    
    #print(town_risk_list_sort)
    
    #Print 10 highest scoring towns
    for town in town_risk_list_sort[:10]:
        #Compose display string
        string = 'Town: ' + str(town[0])
        for i in range(len(string), 40):
            string += ' '
        string += 'Risk: '+ str(town[1])
        print(string)

    #Plot on graph
    plot_towns(town_risk_list_sort)
    
    #Plot 5 highest and lowest
    stationsnew = []
    for station in stations:
        if not station.flood_risk is None:
            stationsnew.append(station)
    
    #Sort station list into score order            
    stationsnew.sort(key=lambda station: station.flood_risk, reverse=True)
    
    for station in stationsnew[:5]:
        dates2, levels2 = fetch_measure_levels(station.measure_id,
                                                 dt=datetime.timedelta(days=2))
        plot_water_levels(station, dates2, levels2)
    
    for station in stationsnew[-2:]:
        dates2, levels2 = fetch_measure_levels(station.measure_id,
                                                 dt=datetime.timedelta(days=2))
        plot_water_levels(station, dates2, levels2)
      
            

if __name__ == "__main__":
    print('***Group 146 Flood Monitoring System***')
    
    run()