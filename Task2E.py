# -*- coding: utf-8 -*-
"""
Created on Fri Jan 27 18:15:18 2017

@author: Chris
"""
import datetime
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.plot import plot_water_levels, plot_multi_water_levels
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_rel_level

def run():
    """Requirements for Task 2E without optional extension"""

    # Build list of stations
    stations = build_station_list()
    update_water_levels(stations)
     
    #List of 5 most at risk stations    
    risk_list = stations_highest_rel_level(stations, 5)
    dt = 10
    
    #Cycle through each station and plot each one individually
    for station in risk_list:       
        #Get past data for each station and plot
        dates, levels = fetch_measure_levels(station.measure_id,
                                             dt=datetime.timedelta(days=dt))
        plot_water_levels(station,dates,levels)


if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")

    # Run Task1A
    run()