# -*- coding: utf-8 -*-
"""
Created on Sun Feb 12 11:37:14 2017

@author: tmull
"""
import datetime
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.datafetcher import fetch_latest_water_level_data
from floodsystem.analysis import polyfit 
from floodsystem.plot import plot_water_levels
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.analysis_tom import extrapolate_polyfit_line
from floodsystem. analysis_tom import assign_tomorrow_flood_risk

def run():
    
    stations = build_station_list()
    
    dt = 31
    
    
    
    for station in stations[:100]:
        dates, levels = fetch_measure_levels(station.measure_id,
                                            dt=datetime.timedelta(days=dt))
        
        rv = assign_tomorrow_flood_risk(station, dt, dates, levels)
        
        print(rv)
    
    

    
   
    
    
   
        
    
if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")

    # Run Task1A
    run()
    

