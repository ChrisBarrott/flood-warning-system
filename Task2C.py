# -*- coding: utf-8 -*-
"""
Created on Wed Feb  1 14:26:22 2017

@author: tmull
"""

from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels

def run():
    

    stations = build_station_list()
    update_water_levels(stations)
    
    N = 10
      
    
    high_risk_stat = stations_highest_rel_level(stations, N)
    
    #Print station and its water level
    for station in high_risk_stat:
        print(station.name, ' ', station.relative_water_level())
    
    
    #for tuple_ in high_risk_stat:
    #    print(tuple_[0].name, ' ', str(tuple_[1]))


if __name__ == "__main__":
    run()