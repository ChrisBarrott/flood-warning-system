# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 22:39:51 2017

@author: tmull
"""

from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number

def run():
    

    stations = build_station_list()
    
    N = 9
    
    river_numstations = rivers_by_station_number(stations, N)
    
    print(river_numstations)


if __name__ == "__main__":
    run()