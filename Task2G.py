# -*- coding: utf-8 -*-
"""
Created on Sun Jan 22 22:39:51 2017

@author: CBarrott
"""

from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.analysis import current_level_frequency, average_level,poly_derivative
from floodsystem.datafetcher import fetch_measure_levels
import datetime
from floodsystem.plot import plot_water_levels,plot_water_level_with_fit
from floodsystem.analysis import polyfit
from floodsystem.geo import stations_by_town, floodrisk_by_town
from random import randint
from floodsystem.utils import sorted_by_key

def run():
    LIMIT = 10

    stations = build_station_list()
    update_water_levels(stations)
    
    #If data is not consistent, remove from the list
    for station in stations:
        if not station.typical_range_consistent():
            stations.remove(station)
    
       
    for station in stations[:LIMIT]: 
        #Get data for past month
        dates31, levels31 = fetch_measure_levels(station.measure_id,
                                             dt=datetime.timedelta(days=31))    
        
        #Get data for past 2 days
        dates2, levels2 = fetch_measure_levels(station.measure_id,
                                             dt=datetime.timedelta(days=2))
    
        #Check data exists
        #if (dates31 and levels31 and dates2 and levels2) is class('list'):
        
        #Compare current level to past data    
        cur_lev_freq_score = current_level_frequency(station, dates31, levels31)
        
        #Compare average level over past 'days' to typical range
        avg_level_score = average_level(station, dates2, levels2)
        
        #Look at gradient of best fit line
        p = 4
        gradient_score = poly_derivative(station, dates2, levels2, p)
        #poly, d0 = polyfit(dates2,levels2,p)
        #plot_water_level_with_fit(station, dates2, levels2, poly)
        #print(gradient_score)
        
        #Average scores from previous functions
        avg_score = (cur_lev_freq_score + avg_level_score + gradient_score)/3
            
        #Set score to station
        station.set_flood_risk(avg_score)
    

    #Gather data together and print
    stations = stations[:LIMIT]

    #Get towns to stations dictionary    
    towns_to_stations = stations_by_town(stations)
    #Get list of (towns, floodrisk) tuples
    town_risk_list = floodrisk_by_town(towns_to_stations)
    #Sort into risk from high to low
    town_risk_list_sort = sorted_by_key(town_risk_list, 1, True)
    
    #print(town_risk_list_sort)
    
    #Print 10 highest scoring towns
    for town in town_risk_list_sort[:10]:
        #Compose display string
        string = 'Town: ' + str(town[0])
        for i in range(len(string), 50):
            string += ' '
        string += 'Risk: '+ str(town[1])
        print(string)
    
            

if __name__ == "__main__":
    print('***Group 146 Flood Monitoring System***')
    
    run()