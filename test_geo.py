# -*- coding: utf-8 -*-
"""
Created on Mon Jan 23 19:40:13 2017

@author: Chris
"""

import pytest
from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance, stations_within_radius, rivers_by_station_number, rivers_with_station, stations_by_river
from floodsystem.station import MonitoringStation

def test_stations_by_distance():
    """Tests stations by distance function"""
   
    #Make a list
    stations = build_station_list()
    cambcentre = (52.2053,0.1218)
    StatDistList = stations_by_distance(stations, cambcentre)
    
    #Compare with known distance to closest station
    assert round(StatDistList[0][2],2) == 0.84


def test_stations_by_radius():
    """Tests stations by radius function"""
   
    #Make a list
    stations = build_station_list()
    cambcentre = (52.2053,0.1218)
    StatRadListName = stations_within_radius(stations,cambcentre, 10)
    
    #Compare with known close station that is first alphabetically
    assert StatRadListName[0] == 'Bin Brook'
    

def test_rivers_by_station_number():
    """Tests rivers_by_station_number function"""
    
    stations = build_station_list()
    
    N = 9
    
    river_numstations = rivers_by_station_number(stations, N)
    
    assert river_numstations[0][1] >= river_numstations[1][1]

    

def test_rivers_with_station ():    
    """Tests rivers_with_station function"""
    
     #Generate 3 stations
    
    
    station1 = MonitoringStation(station_id='a',
                                  measure_id='b',
                                  label='c',
                                  coord=(23,
                                         45),
                                  typical_range=(3,5),
                                  river='camb',
                                  town='cambridge')
    
                                  
    station2 = MonitoringStation(station_id='d',
                                  measure_id='e',
                                  label='f',
                                  coord=(23,
                                         45),
                                  typical_range=(4,2),
                                  river='camb',
                                  town='cambridge') 
                                
                                  
    station3 = MonitoringStation(station_id='g',
                                  measure_id='h',
                                  label='i',
                                  coord=(23,
                                         45),
                                  typical_range=None,
                                  river='River 1',
                                  town='cambridge')           
    
    stations = [station1, station2, station3]
    
    #Build list of stations
    river_list = rivers_with_station(stations)  

    #sort the list into alphabetical order
    a = sorted(river_list)

    #compare with the with the known first station
    assert len(a) == 2
    
def test_staions_by_river ():
    """Tests rivers_with_station function"""
    
    station1 = MonitoringStation(station_id='a',
                                  measure_id='b',
                                  label='c',
                                  coord=(23,
                                         45),
                                  typical_range=(3,5),
                                  river='camb',
                                  town='cambridge')
    
                                  
    station2 = MonitoringStation(station_id='d',
                                  measure_id='e',
                                  label='f',
                                  coord=(23,
                                         45),
                                  typical_range=(4,2),
                                  river='camb',
                                  town='cambridge') 
                                
                                  
    station3 = MonitoringStation(station_id='g',
                                  measure_id='h',
                                  label='i',
                                  coord=(23,
                                         45),
                                  typical_range=None,
                                  river='River 1',
                                  town='cambridge')           
    
    
    stations = [station1, station2, station3]
    
    river_station = stations_by_river(stations)
 
    select_a_river = river_station.get('camb')
    
    select_a_river.sort()
    first_entry = select_a_river[0]
    
    #compare first_entry with the first station alphabetically on the river aire
    assert first_entry == 'c'
    