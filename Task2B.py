# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 15:44:36 2017

@author: tmull
"""

from floodsystem.flood import stations_level_over_threshold
from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels


def run():
    

    stations = build_station_list()
    
    update_water_levels(stations)
    
    
    
    tol = 0.8
    
    risk_list = stations_level_over_threshold(stations, tol)
    for tuple_ in risk_list:
        print(tuple_[0].name, ' ', str(tuple_[1]))
    
    
    
    


if __name__ == "__main__":
    run()