# -*- coding: utf-8 -*-
"""
Created on Fri Feb 17 21:38:56 2017

@author: tmull
"""

import datetime
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.datafetcher import fetch_measure_levels, fetch_latest_water_level_data 
from floodsystem.analysis import (polyfit, poly_extrapolate_risk, 
rel_water_level_risk, assess_validity_poly_curve, current_level_frequency) 
from floodsystem.plot import plot_water_levels
from floodsystem.utils import sorted_by_key
from floodsystem.station import MonitoringStation

def test_assign_tomorrow_flood_risk():
    stations = build_station_list()
    
    dt = 2    
    update_water_levels(stations)    
    list_for_test = []
    
    for station in stations[:10]:
        try:
        
            dates, levels = fetch_measure_levels(station.measure_id,
                                                dt=datetime.timedelta(days=dt))            
            rv = poly_extrapolate_risk(station, dt, dates, levels)            
            list_for_test.append(rv)
                       
        except:
            stations.remove(station)
 
    for i in list_for_test:
        assert i <= 10 and i >=0


def test_cur_level_to_typical_range():
    stations = build_station_list()
    
    update_water_levels(stations)
    list_for_test = []
    
    for station in stations[:5]:      
        rv = rel_water_level_risk(station)
        list_for_test.append(rv)
             
    for i in list_for_test:
        assert i <= 10 and i >=0
        
        
def test_polyfit():
    """Test the polyfit function for a range of data and polynomials"""
    stations = build_station_list()
    update_water_levels(stations)
    
    for station in stations:
        station.set_flood_risk(station.relative_water_level())
        
    stationsnew = []
    for station in stations:
        if not station.flood_risk is None:
            stationsnew.append(station)
        
    stationsnew.sort(key=lambda station: station.flood_risk, reverse=True)
    
    #Test for highest risk, lowest risk and a mid value
    i = 0
    for p in range(1,4): #Do for 4 values of polynomial
        for station in stationsnew:
            if (i==1) or (i==900) or (i==len(stationsnew)-1):   
                dates,levels = fetch_measure_levels(station.measure_id,
                                                     dt=datetime.timedelta(days=2))    
                polyfit(dates,levels,p)
                i += 1   

def test_current_level_frequency():
    """Test current level frequency gives correct outputs"""
    
    stations = build_station_list()
    update_water_levels(stations)
    station = stations[0]
    
    #Current level much higher than others
    levels1 = [-10,-10,-10,-10,-10,-10,-10,-10,-10]
    assert current_level_frequency(station,levels1) == 10
                                  
    #Much lower than others
    levels1 = [100,100,100,100,100,100,100,100,100]
    assert current_level_frequency(station,levels1) == 0

def test_assess_validity_poly_curve():
    
    
    stations = build_station_list()
    update_water_levels(stations)
    
    dt = 2
    
    list_of_averages = []
    
    for station in stations[:5]:
        
        
        
        dates, levels = fetch_measure_levels(station.measure_id,
                                                dt=datetime.timedelta(days=dt))
        
        poly, d0 = polyfit(dates, levels, 4)
        
        
        
        average = assess_validity_poly_curve(dates, levels, poly, dt)
        
        list_of_averages.append(average)
        
        
    for i in list_of_averages:
        assert i >= 0
        
        
            
       
    
    






